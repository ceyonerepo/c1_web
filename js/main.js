window.isAutoScroll = false;
$(function () {
    $('#section')[0].scrollTo(0, 0);
    var docHeight = $(document).outerHeight();
    containerHeight = docHeight - $("#footer").outerHeight();
    $("#section>div").height(containerHeight)
    $('#section').on('scroll', function (event) {
        if ($(event.currentTarget).scrollTop() >= 400) {
            $('.site-header.sticky-top').addClass('black');
        } else {
            $('.site-header.sticky-top').removeClass('black');
        }
        if (!window.isAutoScroll) {
            var currentPosition = event.currentTarget.scrollTop;
            var sectionElements = $(event.currentTarget).find('.section-content');
            var distanceArray = [];
            for (var i = 0; i < sectionElements.length; i++) {
                distanceArray.push(sectionElements[i].offsetTop - currentPosition);
            }
            var nearestElementArray = distanceArray.filter(function (v) {
                return v < 0;
            });
            if (nearestElementArray.length)
                var currentElementIndex = distanceArray.indexOf(nearestElementArray[nearestElementArray.length - 1]);
            else
                var currentElementIndex = distanceArray.indexOf(0);
            var currentElement = $(".site-header .container a")[currentElementIndex];
            var nextElement = $(".site-header .container a")[currentElementIndex + 1];
            var currentContiner = (currentElement.hash == '') ? '#home' : currentElement.hash;
            var currentElementHeight = $(currentContiner).height();
            var nextElementHeight = $(nextElement.hash).height();
            var sectionHeight = $("#section").height();
            var currentSectionTop = sectionElements[currentElementIndex].offsetTop;
            var currentSectionHeight = sectionElements[currentElementIndex].offsetHeight;
            var positionDifference = (currentSectionTop + currentSectionHeight) - currentPosition;
            var isNextElementVisible = positionDifference < ((currentElementHeight / 2) + 50);
            if (isNextElementVisible) {
                var currentElement = nextElement;
            }
            var dataObj = {
                "currentTarget": currentElement
            }
            setActiveElement(dataObj);
        }
    });
    //Smooth Scrolling
    $(".site-header .container a").on("click", function (event) {
        event.preventDefault();
        window.isAutoScroll = true;
        setActivePosition(event);
    });
    document.querySelector('#section').addEventListener(
        'webkitTransitionEnd',
        function (event) {
            window.isAutoScroll = false;
        }, false);
    formValidation();
});

function formValidation() {
    // Fetch all the forms we want to apply custom Bootstrap validation styles to
    var forms = document.getElementsByClassName('needs-validation');
    // Loop over them and prevent submission
    var validation = Array.prototype.filter.call(forms, function (form) {
        form.addEventListener('submit', function (event) {
            if (form.checkValidity() === false) {
                event.preventDefault();
                event.stopPropagation();
            }
            form.classList.add('was-validated');
        }, false);
    });
}

function setActivePosition(event) {
    setActiveElement(event);
    if (event.currentTarget.hash == "") {
        currentElement = $("#home")[0];
    } else {
        currentElement = $(event.currentTarget.hash)[0];
    }
    currentElement.scrollIntoView({
        behavior: 'smooth'
    });
}

function setActiveElement(event) {
    $(".site-header .container a").removeClass('active');
    $(event.currentTarget).addClass('active');
    if (event.currentTarget.hash == "") {
        currentElement = $("#home")[0];
    } else {
        currentElement = $(event.currentTarget.hash)[0];
    }
    $("#section>.section-content").removeClass('active');
    $(currentElement).addClass('active');
}