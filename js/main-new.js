window.isAutoScroll = false;
$(function () {
    $('#section')[0].scrollTo(0, 0);
    var docHeight = $(document).outerHeight();
    containerHeight = docHeight - $("#footer").outerHeight();
    var $pages = $('#section>div');
    var isFirst = true;
    var currentIndex = 0;
    var lastScroll = 0;
    var currentScroll = 0;
    var targetScroll = 1; // must be set to the same value as the first scroll
    $("#section>div").height(containerHeight)
    $('#section').on('scroll', function (event) {
        // get current position
        currentScroll = $("#section").scrollTop();
        if (targetScroll == -1 || isFirst) {
            // no target set, allow execution by doing nothing here
        }
        // still moving
        else if (currentScroll != targetScroll) {
            // target not reached, ignore this scroll event
            return;
        }
        // reached target
        else if (currentScroll == targetScroll) {
            // update comparator for scroll direction
            lastScroll = currentScroll;
            // enable passthrough
            targetScroll = -1;
            // ignore this scroll event
            return;
        }
        // get scroll direction
        var dirUp = currentScroll > lastScroll ? false : true;
        // update index
        currentIndex += (dirUp ? -1 : 1);
        // reached before start, jump to end
        if (currentIndex < 0) {
            isFirst = true;
            return
        }
        // reached after end, jump to start
        else if (currentIndex >= $pages.length) {
            return
        }
        // get scroll position of target
        targetScroll = $pages.eq(currentIndex)[0].offsetTop;
        isFirst = false;
        if (!window.isAutoScroll) {
            doScroll(targetScroll);
            var headers = $("#header").find('.container>a');
            var nextActiveHeader = headers[currentIndex];
            $(this).children().removeClass('active');
            $pages.eq(currentIndex).addClass('active');

            var dataObj = {
                "currentTarget": nextActiveHeader
            }
            setActiveElement(dataObj);
        }
        if (currentIndex > 0) {
            $('.site-header.sticky-top').addClass('black');
        } else {
            $('.site-header.sticky-top').removeClass('black');
        }
    });

    //Smooth Scrolling
    $(".site-header .container a").on("click", function (event) {
        event.preventDefault();
        window.isAutoScroll = true;
        setActiveElement(event);
        if (event.currentTarget.hash == "") {
            currentElement = $("#home")[0];
        } else {
            currentElement = $(event.currentTarget.hash)[0];
        }
        targetScroll = $(currentElement)[0].offsetTop;
        doScroll(targetScroll);
        currentIndex = $(currentElement).index();
    });
    document.querySelector('#section').addEventListener(
        'webkitTransitionEnd',
        function (event) {
            window.isAutoScroll = false;
        }, false);
    formValidation();
});

function doScroll(newScroll) {
    $('#section').animate({
        scrollTop: newScroll
    }, 400);
}

function formValidation() {
    // Fetch all the forms we want to apply custom Bootstrap validation styles to
    var forms = document.getElementsByClassName('needs-validation');
    // Loop over them and prevent submission
    var validation = Array.prototype.filter.call(forms, function (form) {
        form.addEventListener('submit', function (event) {
            if (form.checkValidity() === false) {
                event.preventDefault();
                event.stopPropagation();
            }
            form.classList.add('was-validated');
        }, false);
    });
}

function setActivePosition(event) {}

function setActiveElement(event) {
    $(".site-header .container a").removeClass('active');
    $(event.currentTarget).addClass('active');
    if (event.currentTarget.hash == "") {
        currentElement = $("#home")[0];
    } else {
        currentElement = $(event.currentTarget.hash)[0];
    }
    $("#section>.section-content").removeClass('active');
    $(currentElement).addClass('active');
}