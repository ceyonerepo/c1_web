var express = require("express");
var app = express();
var router = express.Router();
var path = require("path");

app.use("/css", express.static(path.join(__dirname + "/../css")));
app.use("/js", express.static(path.join(__dirname + "/../js")));
app.use("/fonts", express.static(path.join(__dirname + "/../fonts")));
app.use("/icons", express.static(path.join(__dirname + "/../icons")));
app.use("/images", express.static(path.join(__dirname + "/../images")));

app.get("/", function(request, response){
    response.sendFile("index.html", {root : path.join(__dirname + "/../")});
});

app.listen(8500);